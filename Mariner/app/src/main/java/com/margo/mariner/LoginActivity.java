package com.margo.mariner;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.margo.mariner.Validation;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Optional;

import static butterknife.OnTextChanged.Callback.AFTER_TEXT_CHANGED;
import static butterknife.OnTextChanged.Callback.TEXT_CHANGED;

//прописать для поворота экрана !!!!!!!!!!!!!!!!!!!!!!

public class LoginActivity extends AppCompatActivity {

    public  static String LOG_TAG = "LOG";

    public static int MIN_SYMBOLS_COUNT = 6;

    private boolean mIsSigned = false;
    private boolean mIsValid = false;
    private Validation validationData;

    @BindView(R.id.loginEditText)
    EditText loginEdit;

    @BindView(R.id.passwordEditText)
    EditText passwordEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        validationData = new Validation();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
         outState.putString("loginEdit", loginEdit.getText().toString());
        outState.putString("passwordEdit", passwordEdit.getText().toString());
        outState.putString("login", validationData.getLogin());
        outState.putString("password", validationData.getPassword());
        outState.putBoolean("isValid", mIsValid);
        outState.putBoolean("isSigned", mIsSigned);
        Log.d(LOG_TAG, "onSaveInstanceState");
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        if(mIsSigned)
        {
            passwordEdit.setText("");
            loginEdit.setText("");
            mIsValid = false;
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        loginEdit.setText(savedInstanceState.getString("loginEdit"));
        passwordEdit.setText(savedInstanceState.getString("passwordEdit"));
        validationData.setLogin(savedInstanceState.getString("login"));
        validationData.setPassword(savedInstanceState.getString("password"));
        mIsSigned = savedInstanceState.getBoolean("isSigned");
        mIsValid = savedInstanceState.getBoolean("isValid");
    }

    @Optional
    @OnClick({R.id.loginButton, R.id.signInButton})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.loginButton:
                if(loginEdit.getText().length()>MIN_SYMBOLS_COUNT)
                {
                    if (!mIsSigned) {
                        Toast.makeText(getApplicationContext(), "You need to sign in", Toast.LENGTH_SHORT)
                                .show();
                        //if entered data for log in is valid, we save it and insert it in the sign activity
                            validationData.setLogin(loginEdit.getText().toString());
                            validationData.setPassword(passwordEdit.getText().toString());
                            mIsValid = true;
                    } else {
                        if (validationData.getLogin().equals(loginEdit.getText().toString())
                                && validationData.getPassword().equals(passwordEdit.getText().toString())) {
                            Intent goToMainActivity = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(goToMainActivity);
                        } else {
                            Toast.makeText(getApplicationContext(), "Invalid login or password", Toast.LENGTH_SHORT)
                                    .show();
                        }
                    }
                }
                else
                    Toast.makeText(getApplicationContext(), "Minimum 6 symbols", Toast.LENGTH_SHORT)
                            .show();
                break;
            case R.id.signInButton:
                Intent goToSignIn = new Intent(LoginActivity.this, SignInActivity.class);
                if(mIsValid && !mIsSigned) {
                    goToSignIn.putExtra("login", validationData.getLogin());
                    goToSignIn.putExtra("password", validationData.getPassword());
                }
                goToSignIn.putExtra("isValid", mIsValid);
                goToSignIn.putExtra("isSigned", mIsSigned);
                mIsSigned = true;
                startActivity(goToSignIn);
                break;
        }
    }
}
