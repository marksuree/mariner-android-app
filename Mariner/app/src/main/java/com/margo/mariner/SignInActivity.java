package com.margo.mariner;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import butterknife.Optional;

import static butterknife.OnTextChanged.Callback.BEFORE_TEXT_CHANGED;
import static butterknife.OnTextChanged.Callback.TEXT_CHANGED;
import static com.margo.mariner.LoginActivity.LOG_TAG;
import static com.margo.mariner.LoginActivity.MIN_SYMBOLS_COUNT;

public class SignInActivity extends AppCompatActivity {

    private Validation validationData;

    @BindView(R.id.loginEditText)
    EditText loginEdit;

    @BindView(R.id.passwordEditText)
    EditText passwordEdit;

    @BindView(R.id.checkPasswordEditText)
    EditText checkEdit;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);

        validationData = new Validation();

        Intent intent = getIntent();
        if(intent.getExtras().getBoolean("isValid") &&
                !(intent.getExtras().getBoolean("isSigned"))) {
            loginEdit.setText(intent.getStringExtra("login"));
            passwordEdit.setText(intent.getStringExtra("password"));
        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("loginEdit", loginEdit.getText().toString());
        outState.putString("passwordEdit", passwordEdit.getText().toString());
        outState.putString("login", validationData.getLogin());
        outState.putString("password", validationData.getPassword());
        outState.putString("check", checkEdit.getText().toString());
        Log.d(LOG_TAG, "onSaveInstanceState");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        loginEdit.setText(savedInstanceState.getString("loginEdit"));
        passwordEdit.setText(savedInstanceState.getString("passwordEdit"));
        validationData.setLogin(savedInstanceState.getString("login"));
        validationData.setPassword(savedInstanceState.getString("password"));
        checkEdit.setText(savedInstanceState.getString("check"));
    }

    @Optional @OnClick(R.id.signInButton)
    void onClick(View v)
    {
        if (checkEdit.getText().toString().equals(passwordEdit.getText().toString()))
        {
            if(checkEdit.getText().length() >= MIN_SYMBOLS_COUNT) {
                validationData.setLogin(loginEdit.getText().toString());
                validationData.setPassword(passwordEdit.getText().toString());
                Intent goToMain = new Intent(SignInActivity.this, MainActivity.class);
                startActivity(goToMain);
            }
            else
            {
                Toast.makeText(getApplicationContext(), "Minimum 6 symbols", Toast.LENGTH_SHORT)
                        .show();
            }
        }
        else
            Toast.makeText(getApplicationContext(), "Passwords aren't equals", Toast.LENGTH_SHORT)
                    .show();
    }
}
